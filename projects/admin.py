from django.contrib import admin
from projects.models import Project
from tasks.models import Task

# Register your models here.


@admin.register(Project)
class Project(admin.ModelAdmin):
    list_dsplay = (
        "name",
        "description",
        "owner",
    )


@admin.register(Task)
class Task(admin.ModelAdmin):
    list_display = (
        "name",
        "start_date",
        "due_date",
        "is_completed",
        "project",
        "assignee",
    )
